package com.devcamp.rainbow.rainbowrestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainBowColor () {
        ArrayList<String> color = new ArrayList<>();

        color.add("red");
        color.add("orange");
        color.add("yellow");
        color.add("Green");
        color.add("blue");
        color.add("indigo");
        color.add("violet");


        return color;
    }
}
